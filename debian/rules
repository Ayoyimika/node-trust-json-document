#!/usr/bin/make -f

include /usr/share/dpkg/pkg-info.mk

# detect if build targets experimental suite (or is a draft)
DEB_SUITE_EXP = $(filter experimental% UNRELEASED,$(DEB_DISTRIBUTION))

# generate documentation unless nodoc requested
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
DOCS = INITIALIZER.html INITIALIZER.txt README.html README.txt
endif

MOCHA = mocha --reporter tap

# Work around missing node module "json-schema-test-suite"
MOCHA_EXCLUDES += \
 test/JSONSchemaTestSuite.js

override_dh_auto_build: debian/js/json-document.min.js.gz $(DOCS)

override_dh_auto_test:
	$(strip find test -type f -name '*.js' \
		$(patsubst %,-not -path '%',$(MOCHA_EXCLUDES)) \
		-exec $(MOCHA) {} +) \
		$(if $(DEB_SUITE_EXP),|| true)

override_dh_installdocs:
	dh_installdocs --all -- $(DOCS)

debian/js/%.min.js:
	babeljs --no-babelrc src -d lib --presets=@babel/preset-env
	webpack --output-filename json-document.js --mode development
	webpack --mode production
	install --mode a=r,u+w -D --target-directory debian/js \
		-- dist/*

%.gz: %
	pigz --force --keep -11 -- $<
	brotli --force --keep --best --suffix=.brotli -- $<

%.html: %.md
	pandoc --from gfm-raw_html --to html --standalone --output $@ $<

%.txt: %.md
	pandoc --from gfm-raw_html --to plain --output $@ $<

%:
	dh $@

.SECONDARY:
